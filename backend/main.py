from os import path
from datetime import datetime, timedelta

# Hack to get python working properly
import sys
sys.path.append('model')

import api.app
import database_utils
from geography import City
from airquality import AirQualityHandler
from footfall import FootfallHandler
from traffic import TrafficHandler


if __name__ == '__main__':
    york = City.from_folder(path.join('model', 'york'))

    database_utils.clear_database()
    database_utils.geography_to_database(york)

    mock_time = datetime.now().replace(minute=0, second=0, microsecond=0)

    air_quality_handler = AirQualityHandler(filepath="model/york/airquality.csv")
    footfall_handler = FootfallHandler(filepath="model/york/footfall.csv")
    traffic_handler = TrafficHandler(filepath="model/york/traffic_times.csv")

    # list to hold (zone id, date time, price) tuples
    rows = []

    # generate 60 days worth of (zone_id, date time, price) tuples
    for i in range(60 * 24):
        for zid, zone in york.get_zones().items():
            # get data
            footfall = footfall_handler.get_zone_footfall(mock_time, zid)
            traffic = traffic_handler.get_traffic_level(mock_time)
            air_quality = air_quality_handler.get_air_quality(mock_time)
            # update zone price
            zone.tick(traffic, air_quality, footfall)

            # add (zone id, date time, price) to list
            rows.append((zid, mock_time, zone.get_price()))
        
        # increment date time
        mock_time += timedelta(hours=1)

    # add prices to database
    database_utils.insert_prices(rows)

    # run API
    api.app.start_api()

import csv
import random


DAYS = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
MAX_TRAFFIC = 2000
STANDARD_DEVIATION = 150


class TrafficHandler:
    def __init__(self, filepath="york/traffic_times.csv"):
        self._time_multipliers = {
            'monday': [],
            'tuesday': [],
            'wednesday': [],
            'thursday': [],
            'friday': [],
            'saturday': [],
            'sunday': []
        }

        with open(filepath, 'r', newline="") as file:
            dictreader = csv.DictReader(file)

            for row in dictreader:
                for day, multiplier in row.items():
                    self._time_multipliers[day.lower()].append(0.01 * float(multiplier))

    def get_traffic_level(self, dt):
        weekday = DAYS[dt.weekday()]
        hour = dt.hour

        multiplier = self._time_multipliers[weekday][hour]

        return max(0.0, random.gauss(multiplier * MAX_TRAFFIC, multiplier * STANDARD_DEVIATION))


if __name__ == '__main__':
    from datetime import datetime, timedelta

    th = TrafficHandler()

    dt = datetime.now()

    for i in range(24):
        print(dt, th.get_traffic_level(dt))
        dt = dt + timedelta(hours=1)
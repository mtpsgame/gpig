"""
author -- PW
filename -- airquality.py

Supplies "measured" air quality (which actually is data from 2019)
"""

import csv
import random
from collections import namedtuple
from datetime import datetime

DATE_FORMAT = "%d/%m/%Y %H:%M:%S"

# max levels set to 0.75 of the UK's legal maximum annual mean
# (given that York is generally a low pollution area)
MAX_NO2 = 30.0
MAX_NOX = 40.0
MAX_PARTICULATES = 40.0


AirQuality = namedtuple('AirQuality', ['NO2', 'NOX', 'particulates'])


class AirQualityHandler:
    def __init__(self, filepath="york/airquality.csv"):
        self._data = {}

        with open(filepath, 'r', newline="") as file:
            dictreader = csv.DictReader(file)

            for row in dictreader:
                # python uses 00:00:00 for midnight
                # the UK govt appears to use 24:00:00...
                time = row['End Time']
                if '24:00:00' == time:
                    time = '00:00:00'

                date_string = row['End Date'] + ' ' + time
                datetime_ = datetime.strptime(date_string, DATE_FORMAT)

                self._data[datetime_] = row

    def _get_data(self, dt, field, add_noise=True):
        try:
            data = float(self._data[dt][field])
            if add_noise:
                data = random.gauss(data, 0.5)
            return data
        except (ValueError, TypeError):
            return 0

    @staticmethod
    def _round_datetime(dt):
        """
        round datetime object to nearest hour, and set year to 2019
        """
        return dt.replace(year=2019, minute=0, second=0, microsecond=0)

    def get_no2(self, dt):
        dt = self._round_datetime(dt)
        return self._get_data(dt, 'NO2')

    def get_nox(self, dt):
        dt = self._round_datetime(dt)
        return self._get_data(dt, 'NOXasNO2')

    def get_particulates(self, dt):
        dt = self._round_datetime(dt)
        return self._get_data(dt, 'PM10')

    def get_air_quality(self, dt):
        return AirQuality(
            NO2=self.get_no2(dt),
            NOX=self.get_nox(dt),
            particulates=self.get_particulates(dt)
        )


if __name__ == '__main__':
    aqh = AirQualityHandler()
    dt = datetime(2019, 1, 1, 12, 0, 0)
    print(aqh.get_air_quality(dt))

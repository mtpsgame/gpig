import time
import random
import sqlite3
from datetime import datetime, timedelta

from geography import City
from airquality import AirQualityHandler
from footfall import FootfallHandler
from traffic import TrafficHandler

from os import path
import sys
sys.path.append('../')
import database_utils

if __name__ == "__main__":
    york = City.from_folder("york")

    database_utils.clear_database()
    database_utils.geography_to_database(york)

    conn = sqlite3.connect('congestioncharge.db')
    c = conn.cursor()

    # MTP Hack - Tracks the time as it will appear in the finished system, on the hour.
    mock_time = datetime.now().replace(minute=0, second=0, microsecond=0)

    air_quality_handler = AirQualityHandler()
    footfall_handler = FootfallHandler()
    traffic_handler = TrafficHandler()

    while True:
        # Wait 10 seconds for demo
        # Change to 1 hour for live
        time.sleep(10)
        for row in c.execute('select * from Prices'):
            print(row)

        for zid in york._zones:
            zone = york._zones[zid]
            footfall = footfall_handler.get_zone_footfall(mock_time, zid)
            traffic = traffic_handler.get_traffic_level(mock_time)
            air_quality = air_quality_handler.get_air_quality(mock_time)

            zone.tick(traffic, air_quality, footfall)

            c.execute("INSERT INTO Prices (ZoneID, DateTime, Price) VALUES (?, ?, ?)", (zid, mock_time, zone.get_price()))

        mock_time += timedelta(hours=1)

        conn.commit()

    conn.close()

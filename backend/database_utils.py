import sqlite3 as sql

from geography import City


def clear_database():
    """
    Removes all rows from all tables in the database.
    """
    connection = sql.connect('congestioncharge.db')
    cursor = connection.cursor()
    cursor.execute('DELETE FROM Coordinates')
    cursor.execute('DELETE FROM Events')
    cursor.execute('DELETE FROM HistoricalDatasets')
    cursor.execute('DELETE FROM Passes')
    cursor.execute('DELETE FROM Prices')
    cursor.execute('DELETE FROM Zones')
    connection.commit()
    connection.close()


def geography_to_database(city):
    """
    Inserts a 'geography.City' object into the database.
    :param city: The City object to insert into the database.
    """
    if not isinstance(city, City):
        raise ValueError("Supplied zone argument is not a valid Zone object. Actual type is: %s" % type(city))
    connection = sql.connect('congestioncharge.db')
    cursor = connection.cursor()
    zones = city.get_zones()
    for zone in zones.items():
        zone_id = zone[0]
        zone_object = zone[1]
        cursor.execute('INSERT INTO Zones (ID, Name) VALUES (?, ?)', (zone_id, zone_object.get_display_name()))
        for coord in zone_object.get_outline_coordinates():
            cursor.execute('INSERT INTO Coordinates (ZoneID, Longitude, Latitude) VALUES (?, ?, ?)',
                           (zone_id, coord.longitude, coord.latitude))
    connection.commit()
    connection.close()


def insert_price(zid, time, price):
    connection = sql.connect('congestioncharge.db')
    cursor = connection.cursor()

    cursor.execute("INSERT INTO Prices (ZoneID, DateTime, Price) VALUES (?, ?, ?)", (zid, time, price))

    connection.commit()
    connection.close()


def insert_prices(rows):
    connection = sql.connect('congestioncharge.db')
    cursor = connection.cursor()

    cursor.executemany("INSERT INTO Prices (ZoneID, DateTime, Price) VALUES (?, ?, ?)", rows)

    connection.commit()
    connection.close()
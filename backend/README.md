### Installation ###

1. Install python3.7.X and pip3 
2. Change into root directory
3. run "pip(3) install -r requirements.txt" 

This should install all the required packages 
needed to run the flask server

### Running ###

1. run "python(3) flaskr/app.py"

This should start the server on local port 5000